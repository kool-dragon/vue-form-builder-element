'use strict'

const path = require('path')
function resolve(dir) {
  return path.join(__dirname, dir)
}

const name = '工作流' // page title

module.exports = {
  publicPath: '',
  outputDir: 'dist',
  // assetsDir: 'static',
  // indexPath: 'aaa/index.html',
  filenameHashing: false,
  productionSourceMap: false,
  devServer: {
    port: 8081,
    open: false,
    overlay: {
      warnings: false,
      errors: true
    },
    // proxy: {
    //   [process.env.VUE_APP_BASE_API]: {
    //     target: ``,
    //     // target: ``,
    //     changeOrigin: true,
    //     pathRewrite: {
    //       [process.env.VUE_APP_BASE_API]: ''
    //     }
    //   },
    //   [process.env.VUE_APP_ELSE_API]: {
    //     target: ``,
    //     changeOrigin: true,
    //     pathRewrite: {
    //       [process.env.VUE_APP_ELSE_API]: ''
    //     }
    //   }
    // }
  },
  configureWebpack: {
    // provide the app's title in webpack's name field, so that
    // it can be accessed in index.html to inject the correct title.
    name: name,
    resolve: {
      alias: {
        '@': resolve('src')
      }
    }
  },
  chainWebpack(config) {
    // set svg-sprite-loader
    config.module
      .rule('svg')
      .exclude.add(resolve('src/icons'))
      .end()
    config.module
      .rule('icons')
      .test(/\.svg$/)
      .include.add(resolve('src/icons'))
      .end()
      .use('svg-sprite-loader')
      .loader('svg-sprite-loader')
      .options({
        symbolId: 'icon-[name]'
      })
      .end()

    // set preserveWhitespace
    config.module
      .rule('vue')
      .use('vue-loader')
      .loader('vue-loader')
      .tap(options => {
        options.compilerOptions.preserveWhitespace = true
        return options
      })
      .end()
  }
  // module: {
  //   rules: [
  //     {
  //       test: /\.svg$/,
  //       loader: 'svg-sprite-loader'
  //     },
  //     {
  //       test: /\.svg$/,
  //       loader: 'file-loader',
  //       exclude: path.resolve(__dirname, './src/assets/icons') // 不带icon 玩
  //     },
  //     {
  //       test: /\.svg$/,
  //       loader: 'svg-sprite-loader',
  //       include: path.resolve(__dirname, './src/assets/icons') // 只带自己人玩
  //     }
  //   ]
  // }
}
