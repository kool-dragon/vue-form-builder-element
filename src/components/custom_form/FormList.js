import { inputConf } from "./control/Input";
import { selectConf } from "./control/Select";
import { radioConf } from "./control/Radio";
import { checkBoxConf } from "./control/CheckBox";
import { cascaderConf } from "./control/Cascader";
import { textareaConf } from "./control/Textarea";
import { titleConf } from "./control/Title";
import { hrConf } from "./control/Hr";
import { pConf } from "./control/P";
import { datePickerConf } from './control/DatePicker'
import { buttonConf } from './control/Button';
import { uploadsConf } from './control/Uploads';
import { switchConf } from './control/Switch';
import { sliderConf } from './control/Slider';
import { brConf } from './control/Br';

// const formList = {
//   title: titleConf,
//   hr: hrConf,
//   br:brConf,
//   button: buttonConf,
//   p: pConf,
//   myswitch: switchConf,
//   slider:sliderConf,
//   input: inputConf,
//   select: selectConf,
//   radio: radioConf,
//   checkbox: checkBoxConf,
//   textarea: textareaConf,
//   datepicker: datePickerConf,
//   cascader: cascaderConf,
//   uploads: uploadsConf,
// };
const list_arr = [
  {  name:'标题',ele:'title',obj:titleConf},
  {  name:'分割线',ele:'hr',obj:hrConf},
  {  name:'换行符',ele:'br',obj:brConf},
  {  name:'按钮',ele:'button',obj:buttonConf},
  {  name:'文本',ele:'p',obj:pConf},
  {  name:'开关',ele:'myswitch',obj:switchConf},
  {  name:'滑块',ele:'slider',obj:sliderConf},
  {  name:'输入框',ele:'input',obj:inputConf},
  {  name:'下拉框',ele:'select',obj:selectConf},
  {  name:'单选',ele:'radio',obj:radioConf},
  {  name:'多选',ele:'checkbox',obj:checkBoxConf},
  {  name:'多行输入框',ele:'textarea',obj:textareaConf},
  {  name:'时间选择器',ele:'datepicker',obj:datePickerConf},
  {  name:'级联选择器',ele:'cascader',obj:cascaderConf},
  {  name:'上传',ele:'uploads',obj:uploadsConf},
]
// let list_arr = [];

// for (let i in formList) {
//   list_arr.push({
//     ele: i,
//     obj: formList[i]
//   });
// }
export default list_arr;
