import ItemIcon from "./ItemIcon";
import input from "./control/Input";
import checkbox from "./control/CheckBox";
import radio from "./control/Radio";
import select from "./control/Select";
import textarea from "./control/Textarea";
import cascader from "./control/Cascader";
import title from "./control/Title";
import hr from "./control/Hr";
import p from "./control/P";
import uploads from "./control/Uploads";
import datepicker from "./control/DatePicker";
import button from "./control/Button";
import myswitch from "./control/Switch";
import slider from "./control/Slider";
import br from "./control/Br";

import trigger from "./config/trigger";

const form_item = {
  title,
  hr,
  p,
  input,
  select,
  radio,
  checkbox,
  datepicker,
  cascader,
  uploads,
  textarea,
  button,
  myswitch,
  slider,
  br,
};

export default {
  name: "renders",
  props: {
    // 当前控件的类型
    ele: { type: String, default: "input" },
    // 当前控件的配置
    obj: {
      type: Object,
      default() {
        return {};
      },
    },
    // 当前控件的index,config 和 delete 会用到
    index: { type: Number, default: 0 },
    // 是否显示配置按钮
    configIcon: { type: Boolean, default: false },
    // 当前被clone控件列表
    sortableItem: {
      type: Array,
      default() {
        return [];
      },
    },
    // 配置渲染  还是现实渲染 显示渲染
    renderPattern: { type: String, default: "example" }, //example config show
  },
  render(h) {
    // 获取当前控件渲染(数组包着的渲染函数)
    const arr = (form_item[this.ele.toLowerCase()] && form_item[this.ele.toLowerCase()](this, h)) || [];
    // 显示配置按钮并且控件允许被配置
    const item_icon = this.configIcon && this.obj.config ? ItemIcon(this, h) : [];
    // 已被绑定name,且require为必填,视为校验字段
    const validate = !!this.obj.name && !!this.obj.require;
    // 如果不是 Title Hr P 中的其中一个 需要FormItem
    if (["title", "hr", "p"].indexOf(this.ele.toLowerCase()) < 0) {
      if (this.ele.toLowerCase() === "br") { //因为换行符有些特殊,在example中显示文字,在config中显示可以配置,在show中渲染回车
        if (this.renderPattern === "example") {
          return h(
            "span",
            {
              class: {
                items: true,
              },
            },
            arr
          );
        } else if (this.renderPattern === "config") {
          return h(
            "div",
            {
              class: {
                items: true,
              },
              style: {
                width:'100px',
                display: this.obj.inlineBlock ? "inline-block" : "block",
                position: "relative",
              },
            },
            arr.concat(item_icon)
          );
        } else if (this.renderPattern === "show") {
          return h("br");
        }
      }
      if (this.ele.toLowerCase() === "button" && this.obj.inlineBlock) {
        return h(
          "div",
          {
            class: {
              items: true,
            },
            style: {
              // 是否显示行内元素
              display: "inline-block",
              position: "relative",
            },
          },
          arr.concat(item_icon)
        );
      } else { //如果是一个表单控件
        let FormItem = {
          class: {
            items: true
          },
          props: {
            label: this.obj.label ? (this.obj.label || this.ele) + "：" : "",
            // 指定验证name
            prop: this.obj.name || "temp",
            // 验证规则
            rules: {
              required: validate,
              message: this.obj.ruleError || "该项为必填项",
              trigger: trigger[this.obj.type],
              validator: (rule, value, callback) => {
                // 没有配置按钮并且允许验证
                if (validate && (Array.isArray(value) ? !value.length : !value)) {
                  callback(new Error("该项为必填项"));
                } else {
                  callback();
                }
              },
            },
          },
        };
        return h("el-form-item", FormItem, arr.concat(item_icon));
      }
    } else {// 如果是 Title Hr P 中的其中一个
      return h(
        "div",
        {
          style: {
            display: this.obj.inlineBlock ? "inline-block" : "block",
            position: "relative",
          },
          class: {
            items: true,
          },
        },
        arr.concat(item_icon)
      );

    }
  },
};
