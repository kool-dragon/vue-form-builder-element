export default (_self, h) => {
  return [
    h("el-input", {
      style:{
        width:  _self.obj.width + "px"
      },
      props:{
        disabled:!_self.obj.name
      },
      attrs: {
        type: "textarea",
        placeholder: _self.obj.placeholder,
        value: _self.obj.value,
        maxlength: _self.obj.maxLength || 200,
        autosize:{  minRows: _self.obj.rowsNumber, maxRows: _self.obj.rowsNumber }
      },
      on: {
        input (value) {
          _self.obj.value = value;
        }
      }
    })
  ];
};

export let textareaConf = {
  // 对应数据库内类型
  type: 'textarea',
  // 是否可配置
  config: true,
  // 控件左侧label内容
  label: '文本域',
  placeholder: '提示信息',
  // 是否显示行内元素
  // inlineBlock: false,
  // 最大长度
  maxLength: 200,
  // 是否必填
  require: false,
  // 文本域行最大高 
  rowsNumber: 5,
  // 绑定的值
  value: "",
  // 表单name
  name: '',
  // 验证错误提示信息
  ruleError: '该字段不能为空',
  width: 400,
}
