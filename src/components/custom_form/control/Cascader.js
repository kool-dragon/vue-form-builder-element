import area from '../config/area'
export default (_self, h) => {
  return [
    h("Cascader", {
      props: {
        width: _self.obj.width +'px',
        placeholder: _self.obj.placeholder,
        multiple: _self.obj.multiple,
        self: _self,
        data:area,
        value:_self.obj.value,
        filterable:_self.obj.filterable,
        showAllLevels:_self.obj.showAllLevels,
      },
    })
  ];
};

export let cascaderConf = {
  // 对应数据库内类型
  type: 'cascader',
  // 是否可配置
  config: true,
  // 控件左侧label内容
  label: '级联选择器',
  placeholder: '这是一个级联选择器',
  // 是否显示行内元素
  // inlineBlock: false,
  // 是否必填
  require: false,
  // 是否多选
  multiple: true,
  // 表单name
  name: '',
  width:400,
  // 绑定的值
  value: [],
  // 验证错误提示信息
  ruleError: '该选项不能为空',
  //是否可搜索
  filterable:false,
  //仅选择最后一级
  showAllLevels:false
}