export default (_self, h) => {
  return [
    h(
      "el-select", {
        attrs: {
          placeholder: _self.obj.placeholder,
          value: _self.obj.value
        },
        props:{
          disabled:!_self.obj.name
        },
        style:{
          width: _self.obj.width + "px"
        },
        on: {
          change (item) {
            console.log(item,'item')
            _self.obj.value = item;
          }
        }
      },
      _self.obj.dataList.map(v => {
        return h(
          "el-option", {
            attrs: {
              value: v.value,
              label: v.label
            },
          },
          // v.label_name
        );
      })
    )
  ];
};

export let selectConf = {
  // 对应数据库内类型
  type: 'select',
  // 是否可配置
  config: true,
  // 控件左侧label内容
  label: '选择框',
  placeholder: '请输入提示信息',
  // 是否显示行内元素
  // inlineBlock: false,
  // 是否必填
  require: false,
  //数据类型: 0. 手动添加 1.数据字典
  selectDataType:0,
  //数据字典的id
  selectDataId:null,
  // 选项内数据
  dataList: [
    {value:  1,label: "选项1"},
    {value:  2,label: "选项2"},
    {value:  3,label: "选项3"},
    {value:  4,label: "选项4"},
  ],
  // 绑定的值
  value: '',
  // 表单name
  name: '',
   // 宽度
  width: 400,
  // 验证错误提示信息
  ruleError: '请选择',
}
