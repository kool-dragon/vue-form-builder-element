export default (_self, h) => {
  return [
    h('p', {
      style: {
        'position':'relative',
        'margin-bottom': _self.obj.marginBottom + 'px',
        'margin-top': _self.obj.marginTop + 'px',
        'margin-left': _self.obj.marginLeft + 'px',
        'margin-right': _self.obj.marginRight + 'px',
        'left': _self.obj.left + 'px',
        'right': _self.obj.right + 'px',
        'color': _self.obj.color || "#000",
        'font-size': _self.obj.fontSize + 'px',
        'line-height': _self.obj.lineHeight + 'px',
        'font-weight': _self.obj.fontWeight,
        'text-align': _self.obj.textAlign
      },
      domProps: {
        innerHTML: _self.obj.pText
      }
    })
  ]
}

export const pConf = {
  type:'p',
  // 是否显示行内元素
  config: true,
  pText: '文本标签标签文本标签文本标签文本标签',
  color: '#000',
  marginTop: 0,
  marginBottom: 24,
  marginLeft: 0,
  marginRight: 0,
  left:0,
  right:0,
  fontSize:'16',
  lineHeight:'21',
  fontWeight:'400',
  textAlign:'left',
  // 是否显示行内元素
  inlineBlock: true
}  
