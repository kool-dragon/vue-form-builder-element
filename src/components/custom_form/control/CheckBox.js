export default (_self, h) => {
  return [
    h("el-checkbox-group", {
      props: {
        value: _self.obj.value || [],
        disabled:!_self.obj.name
      },
      nativeOn:{
        click(event){
          if(event.target.tagName == 'INPUT'){
            let index = _self.obj.value.indexOf(event.target.getAttribute('value'))
            if(index != -1){
              _self.obj.value.splice(index,1)
            }else{
              _self.obj.value.push(event.target.getAttribute('value'))
            }
          }
        }
      }
    }, _self.obj.dataList.map(v => {
      return h("el-checkbox", {
        props: {
          label: v.value
        },
      }, v.label)
    }))
  ];
};

export let checkBoxConf = {
  // 对应数据库内类型
  type: 'checkbox',
  // 是否可配置
  config: true,
  // 控件左侧label内容
  label: '多选框',
  // 是否显示行内元素
  // inlineBlock: false,
  // 是否必填
  require: false,
  // 绑定的值
  value: [],
  // 选项内数据
  dataList: [
    {value:  '1',label: "多选框1"},
    {value:  '2',label: "多选框2"},
    {value:  '3',label: "多选框3"},
    {value:  '4',label: "多选框4"},
  ],
  // 表单name
  name: '',
  // 验证错误提示信息
  ruleError: '该选项不能为空',
}
