export default (_self, h) => {
  return [
    h("Uploads", {
      props: {
        action: _self.obj.action,
        limit: _self.obj.limit,
        multiple: _self.obj.multiple,
        maxSize: _self.obj.maxSize,
        uploadTip: _self.obj.uploadTip,
        self:_self
      },
      on: {
        handleUploadsValue(arr) {
          _self.obj.value = arr;
        },
      },
    }),
  ];
};

export const uploadsConf = {
  type:'uploads',
  // 是否可配置
  config: true,
  // 上传文件最大限制
  maxSize: null,
  // 是否允许多文件上传
  multiple: true,
  // 上传地址
  action: "",
  // 最大允许上传个数
  limit: 0,
  // 是否必填
  require: false,
  // 控件左侧label内容
  label: "上传控件",
  // 绑定的值
  value: [],
  // 表单name
  name: "",
  uploadTip:'',
  // 验证错误提示信息
  ruleError: "请上传文件",
};
