export default (_self, h) => {
  return [
    h('DataPicker', {
      props: {
        placeholder: _self.obj.placeholder,
        value: _self.obj.value,
        valueFormat:_self.obj.valueFormat,
        datetype: _self.obj.datetype,
        format: _self.obj.format,
        self:_self,
        width:_self.obj.width + 'px',
      }
    })
  ]
}


export let datePickerConf = {
  type:'datepicker',
  // 对应数据库内类型
  datetype: 'date',
  // 是否可配置
  config: true,
  // 控件左侧label内容
  label: '日期时间选择器',
  placeholder: '请选择日期时间',
  // 是否显示行内元素
  // inlineBlock: false,
  // 是否必填
  require: false,
  // 表单name
  name: '',
  // 绑定的值
  value: "",
  // 验证错误提示信息
  ruleError: '选项不能为空',
  // 绑定值的格式  Date对象  yyyy-MM-dd  timestamp
  valueFormat: "",
  width:400,
  // 是否需要时分
  format: 'yyyy-MM-dd HH:mm:ss'
}
