export default (_self, h) => {
  return [
    h('h' + (_self.obj.level || 3), {
      style: {
        'position':'relative',
        'margin-bottom': _self.obj.marginBottom + 'px',
        'margin-top': _self.obj.marginTop + 'px',
        'margin-left': _self.obj.marginLeft + 'px',
        'margin-right': _self.obj.marginRight + 'px',
        'left': _self.obj.left + 'px',
        'right': _self.obj.right + 'px',
        'color': _self.obj.color || "#000",
        'text-align': _self.obj.textAlign
      },
      domProps: {
        innerHTML: _self.obj.label || "Title"
      }
    })
  ]
}

export let titleConf = {
  type:'title',
  // 是否可配置
  config: true,
  // 控件文本显示内容
  label: '标题',
  // h标签等级（1-6）
  level: 1,
  color: '#000',
  marginTop: 24,
  marginBottom: 24,
  marginLeft: 0,
  marginRight: 0,
  left:0,
  right:0,
  textAlign:'left'
}
