export default (_self, h) => {
  return [
    h("el-slider", {
      props: {
        value: _self.obj.value,
        disabled: !_self.obj.name,
        min: _self.obj.min,
        max: _self.obj.max,
        step: _self.obj.step,
      },
      style: {
        width: _self.obj.width + "px",
      },
      on: {
        change: function (value) {
          _self.obj.value = value;
        },
        input: function (value) {
          _self.obj.vsalue = value;
        }
      },
    }),
  ];
};

export let sliderConf = {
  // 对应数据库内类型
  type: "slider",
  // 是否可配置
  config: true,
  // 控件左侧label内容
  label: "滑块",
  value: 5,
  // 表单name
  name: "",
  width: 400,
  min: 0,
  max: 10,
  step: 1, //步长
  // 是否必填
  require: false,
};
