export default (_self, h) => {
    return [
      h('span', {
        domProps: {
          innerHTML: '换行符'
        }
      })
    ]
  }
export const brConf = {
    type:'br',
    config: true,  // 是否可配置
    inlineBlock: false
}