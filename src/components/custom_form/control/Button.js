export default (_self, h) => {
    return [
      h('el-button', {
        domProps: {
          innerHTML: _self.obj.buttonName || "按钮"
        },
        props:{
          type: _self.obj.buttonType,
          size: _self.obj.buttonSize,
        },
        style:{
          'margin-bottom': _self.obj.marginBottom + 'px',
          'margin-top': _self.obj.marginTop + 'px',
          'margin-left': _self.obj.marginLeft + 'px',
          'margin-right': _self.obj.marginRight + 'px',
        },
        on: {
          click: function() {
            _self.$emit('clickHandler',_self.obj.buttonName,_self.obj.ability)
          }
        }
      })
    ]
  }
export const buttonConf = {
    type:'button',
    buttonName: "自定义按钮",
    buttonSize: 'default',
    config: true,  // 是否可配置
    marginTop: 0,
    marginBottom: 22,
    marginLeft: 0,
    marginRight: 5,
    buttonType:"primary",
    ability:'submit',
    // 是否显示行内元素
    inlineBlock: true
}


