export default (_self, h) => {
  return [
    h("el-radio-group", {
      attrs: {
        "value": _self.obj.value
      },
      props:{
        disabled:!_self.obj.name
      },
      nativeOn:{
        click(event){
          if(event.target.tagName == 'INPUT'){
            _self.obj.value = event.target.getAttribute('value')
          }
        }
      }
    }, _self.obj.dataList.map(v => {
      return h("el-radio", {
        attrs: {
          label: v.value
        }
      }, v.label)
    }))
  ];
};

export let radioConf = {
  // 对应数据库内类型
  type: 'radio',
  // 是否可配置
  config: true,
  // 控件左侧label内容
  label: '单选框',
  // 是否显示行内元素
  // inlineBlock: false,
  // 是否必填
  require: false,
  // 绑定的值
  value: '',
  // 选项内数据
  dataList: [{ "value": "1", "label": "单选框1" }, { "value": "2", "label": "单选框2" },{ "value": "3", "label": "单选框3" }],
  // 表单name
  name: '',
  // 验证错误提示信息
  ruleError: '请选择',
}
