export default (_self, h) => {
  return [
    h('hr', {
      style: {
        'margin-bottom': _self.obj.marginBottom + 'px',
        'margin-top': _self.obj.marginTop + 'px',
        'margin-left': _self.obj.marginLeft + 'px',
        'margin-right': _self.obj.marginRight + 'px',
      }
    })
  ]
}

export const hrConf = {
  type:'hr',
  // 是否可配置
  config: true,
  marginTop: 0,
  marginBottom: 24,
  marginLeft: 0,
  marginRight: 0,
}
