export default (_self, h) => {
    return [
      h("el-switch", {
        props:{
            value:_self.obj.value,
            disabled:!_self.obj.name,
            'active-color':_self.obj.openColor,
            'inactive-color':_self.obj.closeColor
        },
        on: {
          change: function(value) {
            _self.obj.value = value;
          }
        }
      })
    ];
  };
  
  
  export let switchConf = {
    // 对应数据库内类型
    type: 'switch',
    // 是否可配置
    config: true,
    // 控件左侧label内容
    label: 'switch开关',
    openColor:'#13ce66',
    closeColor:'#ff4949',
    value: false,
    // 表单name
    name: '',
  // 是否必填
    require:false,
  }
  