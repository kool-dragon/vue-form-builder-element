export default (_self, h) => {
  return [
    h("el-input", {
      style:{
        width:  _self.obj.width + "px"
      },
      props:{
        disabled:!_self.obj.name
      },
      attrs: {
        placeholder: _self.obj.placeholder || "这是一个输入框配置name属性后才能输入数据",
        maxlength: parseInt(_self.obj.maxLength) || 20,
        value: _self.obj.value || ""
      },
      on: {
        input: function(value) {
          _self.obj.value = value;
        }
      }
    })
  ];
};


export let inputConf = {
  // 对应数据库内类型
  type: 'input',
  // 是否可配置
  config: true,
    // 输入框宽度
  width: '400',
  // 控件左侧label内容
  label: '输入框',
  placeholder: '',
  // 是否显示行内元素
  // inlineBlock: false,
  // 是否必填
  require: false,
  // 最大长度
  maxLength: 20,
  value: '',
  // 表单name
  name: '',
  // 验证错误提示信息
  ruleError: '该字段不能为空',

}
