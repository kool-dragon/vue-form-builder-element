export const dateTimeType = [
    { label : '日期',value:'date'},
    { label : '日期时间',value:'datetime'},
    { label : '年',value:'year'},
    { label : '月', value:'month'},
    { label : '多个日期',value:'dates'},
    { label : '周',value:'week'},
    { label : '日期时间范围',value:'datetimerange'},
    { label : '日期范围',value:'daterange'},
    { label : '月份范围',value:'monthrange'},
  ]