export const buttonTypeList = [
    { label : '默认按钮',value:'default'},
    { label : '主按钮', value:'primary'},
    { label : '文字按钮',value:'text'},
    { label : '信息按钮',value:'info'},
    { label : '成功按钮',value:'success'},
    { label : '警告按钮',value:'warning'},
    { label : '危险按钮',value:'danger'},
  ]

  export const buttonSizeList = [
    { label : '默认按钮',value:'default'},
    { label : '中等按钮', value:'medium'},
    { label : '小型按钮',value:'small'},
    { label : '超小按钮',value:'mini'},
  ]
