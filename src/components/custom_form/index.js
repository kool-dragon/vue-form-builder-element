import render from "./Render";
import DataPicker from './components/DataPicker.vue';
import Cascader from './components/Cascader.vue';
import Uploads from './components/Uploads.vue';
import preview from './Preview';
  //注册配置组件
import TitleConfig from './controlConfig/TitleConfig.vue';
import HrConfig from './controlConfig/HrConfig.vue';
import BrConfig from './controlConfig/BrConfig.vue';
import ButtonConfig from './controlConfig/ButtonConfig.vue';
import PConfig from './controlConfig/PConfig.vue';
import SwitchConfig from './controlConfig/SwitchConfig.vue';
import SliderConfig from './controlConfig/SliderConfig.vue';
import InputConfig from './controlConfig/InputConfig.vue';
import SelectConfig from './controlConfig/SelectConfig.vue';
import RadioConfig from './controlConfig/RadioConfig.vue';
import CheckBoxConfig from './controlConfig/CheckBoxConfig.vue';
import TextareaConfig from './controlConfig/TextareaConfig.vue';
import DataPickerConfig from './controlConfig/DataPickerConfig.vue';
import CascaderConfig from './controlConfig/CascaderConfig.vue';
import UploadsConfig from './controlConfig/UploadsConfig.vue';

// const customForm = {
//   render,
//   Uploads,
//   preview
// };

const install = function(Vue, opts = {}) {
  //注册渲染组件
  Vue.component(render.name, render);
  Vue.component(Uploads.name, Uploads);
  Vue.component(DataPicker.name, DataPicker);
  Vue.component(Cascader.name, Cascader);
  // Vue.component(preview.name, preview);
  //注册配置组件
  Vue.component(TitleConfig.name, TitleConfig);
  Vue.component(HrConfig.name, HrConfig);
  Vue.component(BrConfig.name, BrConfig);
  Vue.component(ButtonConfig.name, ButtonConfig);
  Vue.component(PConfig.name, PConfig);
  Vue.component(SwitchConfig.name, SwitchConfig);
  Vue.component(SliderConfig.name, SliderConfig);
  Vue.component(InputConfig.name, InputConfig);
  Vue.component(SelectConfig.name, SelectConfig);
  Vue.component(RadioConfig.name, RadioConfig);
  Vue.component(CheckBoxConfig.name, CheckBoxConfig);
  Vue.component(TextareaConfig.name, TextareaConfig);
  Vue.component(DataPickerConfig.name, DataPickerConfig);
  Vue.component(CascaderConfig.name, CascaderConfig);
  Vue.component(UploadsConfig.name, UploadsConfig);
  
  
  
};

// if (typeof window !== 'undefined' && window.Vue) {
//   install(window.Vue);
// }

// export default Object.assign(customForm, { install });
export default {install};
