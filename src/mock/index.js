import Qs from 'qs'
import Mock from 'mockjs' // 引入mockjs
// const Random = Mock.Random // Mock.Random 是一个工具类，用于生成各种随机数据

function getData() {
  return   {
    code: 0,
    data: [
      { id: 1, label: '水果' },
      { id: 2, label: '蔬菜' },
      { id: 3, label: '海鲜' },
    ]
  }
}

function getDataById(res) {
  const { id } = JSON.parse(res.body)
  if(id===1){
    return   {
      code: 0,
      data: [
        { value: 1, label: '菠萝' },
        { value: 2, label: '苹果' },
        { value: 3, label: '草莓' },
        { value: 4, label: '凤梨' }
      ]
    }
  }
  if(id===2){
    return   {
      code: 0,
      data: [
        { value: 1, label: '白菜' },
        { value: 2, label: '西红柿' },
        { value: 3, label: '生菜' },
        { value: 4, label: '黄瓜' }
      ]
    }
  }
  if(id===3){
    return   {
      code: 0,
      data: [
        { value: 1, label: '龙虾' },
        { value: 2, label: '鲍鱼' },
        { value: 3, label: '大闸蟹' },
        { value: 4, label: '章鱼' }
      ]
    }
  }
}


Mock.mock('/data/getData', 'post', getData)
Mock.mock('/data/getDataById', 'post', getDataById)
export default Mock